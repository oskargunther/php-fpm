FROM phpdockerio/php71-fpm:latest

ENV DEBIAN_FRONTEND noninteractive
# Install selected extensions and other stuff
RUN apt-get update \
    && apt-get -y --no-install-recommends install php7.1-amqp php7.1-mysql php7.1-redis php7.1-imap php7.1-gd php7.1-intl php7.1-mbstring php7.1-bcmath curl php7.1-curl php7.1-soap php-imagick ghostscript wkhtmltopdf openssl build-essential xorg libssl-dev xvfb supervisor \
    && apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

RUN echo "xvfb-run -a -s \"-screen 0 640x480x16\" wkhtmltopdf \"\$@\"" > /usr/local/bin/wkhtmltopdf.sh
RUN chmod a+x /usr/local/bin/wkhtmltopdf.sh
RUN usermod -u 1000 www-data
COPY overrides.conf /etc/php/7.1/fpm/pool.d/z-overrides.conf
COPY imagemagick-policy.xml /etc/ImageMagick-6/policy.xml
ADD php-ini-overrides.ini /etc/php/7.1/fpm/conf.d/99-overrides.ini

#WORKDIR "/vagrant/repos"
